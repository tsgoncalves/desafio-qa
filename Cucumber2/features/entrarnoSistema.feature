# language: pt
Funcionalidade: EntrarNoSistema
    O dispositivo deve permitir o cadastro de novas contas.
 
Cenário: Novo Usuário
    Dado que estou na primeira tela apos a instalação do app
    Quando digitar um numero de telefone celular não cadastrado
    E pressionar o botão referente a entrar
    E a tela principal é exibida
 
Cenário: Usuário Já possui cadastro
    Dado que estou na primeira tela apos a instalação do app
    Quando digitar um numero de telefone celular cadastrado
    E pressionar o botão referente a entrar
    Então então devo ser notificado que já possuo uma conta
    E a tela principal é exibida