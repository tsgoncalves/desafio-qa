require 'calabash-android/calabash_steps'

Dado(/^que estou na primeira tela apos a instalação do app$/) do
  wait_for_element_exists("* text:'Concordar e continuar'")
  touch("* text:'Concordar e continuar'")
end

Quando(/^digitar um numero de telefone celular não cadastrado$/) do
  wait_for_element_exists("*id:`tel`")
  wait_for_element_exists("*id:`ddd`")
  touch("*id:`ddd`")
  keyboard_enter_text("55")
  touch("*id:`tel`")
  keyboard_enter_text("11982735240")
  
end
Dado(/^pressionar o botão referente a entrar "([^"]*)"$/) do 
  wait_for_element_exists("* text:'OK'")
  touch("* text:'OK'")
  wait_for_element_exists("* text:'Digite o Codigo'")
end

Dado(/^a tela principal é exibida$/) do
  wait_for_element_exists("* text:'WhatsApp'")
end

Dado(/^que estou na primeira tela apos a instalação do app$/) do
  wait_for_element_exists("* text:'Concordar e continuar'")
  touch("* text:'Concordar e continuar'")
end

Quando(/^ digitar um numero de telefone celular cadastrado$/) do
  wait_for_element_exists("*id:`tel`")
  wait_for_element_exists("*id:`ddd`")
  touch("*id:`ddd`")
  keyboard_enter_text("55")
  touch("*id:`tel`")
  keyboard_enter_text("11982735240")
  
end
Dado(/^pressionar o botão referente a entrar "([^"]*)"$/) do 
  wait_for_element_exists("* text:'OK'")
  touch("* text:'OK'")
  wait_for_element_exists("* text:'Digite o Codigo'")
end

Dado(/^pressionar o botão referente a entrar "$/) do 
  wait_for_element_exists("* text:'OK'")
  touch("* text:'OK'")
  wait_for_element_exists("* text:'Digite o Codigo'")
end

Dado(/^a tela principal é exibida$/) do
  wait_for_element_exists("* text:'WhatsApp'")
end

Dado(/^que estou na tela de envio de mensagem$/) do
  wait_for_element_exists("* text:'Digite aqui'")
end
Quando(/^digitar uma mensagem alfanumérica$/) do
  wait_for_element_exists("* text:'Digite aqui'")
  touch("* text:'Digite aqui'")
  String msg = "Oi tudo bem?"
   keyboard_enter_text(msg)
end
Dado(/^pressionar para enviar$/) do
  touch("* id:'send'")
end
Entao(/^verifico que a mensagem é enviada com sucesso$/) do
  wait_for_element_exists("* id:'message_text'")
  accert(msg == "* id:'message_text'")
end

Dado(/^que estou na tela de envio de mensagem$/) do
  wait_for_element_exists("* text:'Digite aqui'")
end
Quando(/^digitar uma mensagem alfanumérica$/) do
  wait_for_element_exists("* text:'Digite aqui'")
  touch("* text:'Digite aqui'")
  String msg = "Oi tudo bem?"
   keyboard_enter_text(msg)
end
Dado(/^pressionar para enviar$/) do
  touch("* id:'send'")
end
Entao(/^verifico que a mensagem é enviada com sucesso$/) do
  wait_for_element_exists("* id:'message_text'")
  accert(msg == "* id:'message_text'")
  wait_for_element_exists("* id:'status'")
end