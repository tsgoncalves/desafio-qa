# language: pt
Funcionalidade: Enviar Mensagens
    O Programa precisa permitir o envio de mensagens entre dois dispositivos.
 
Cenário: Enviar a Mensagem para outro dispositivo
    Dado que estou na tela de envio de mensagem
    Quando digitar uma mensagem alfanumérica
    E pressionar para enviar
    Então verifico que a mensagem é enviada com sucesso
 
Cenário: Enviar a Mensagem para um dispositivo desligado
    Dado que estou na tela de envio de mensagem alfanumérica
    Quando digitar mensagem 
    E pressionar o envio
    Então então devo ver um sinal que a mensagem foi enviada