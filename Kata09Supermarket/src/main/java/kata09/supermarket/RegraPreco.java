package kata09.supermarket;

import java.util.HashMap;
import java.util.Map;


public class RegraPreco {
    private Map<String, PricingRule> pricingRules = new HashMap<String, PricingRule>();

    public void addRegra(String item, PricingRule pricingRule) {
        pricingRules.put(item, pricingRule);
    }

    public int calcularPreco(String item, int amount) {
        PricingRule pricingRule = pricingRules.get(item);
        return pricingRule.calcularpreco(amount);
    }
}
