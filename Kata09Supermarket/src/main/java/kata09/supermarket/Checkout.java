package kata09.supermarket;

import java.util.ArrayList;
import java.util.List;


public class Checkout {
    private RegraPreco regraPreco;
    private List<ScannedItem> scannedItems = new ArrayList<ScannedItem>();

    public Checkout(RegraPreco regraPreco) {
        this.regraPreco = regraPreco;
    }

    public void scan(String item) {
        ScannedItem scannedItem = findAlreadyScannedItem(item);

        if (scannedItem == null) {
            scannedItem = new ScannedItem(item);
            this.scannedItems.add(scannedItem);
        }

        scannedItem.amount += 1;
    }

    private ScannedItem findAlreadyScannedItem(String item) {
        for (ScannedItem i : this.scannedItems) {
            if (i.item.equals(item)) {
                return i;
            }
        }

        return null;
    }

    public int getTotal() {
        int total = 0;

        for (ScannedItem scannedItem : scannedItems) {
            total += regraPreco.calcularPreco(scannedItem.item, scannedItem.amount);
        }

        return total;
    }

    private static class ScannedItem {
        public int amount = 0;
        public String item;

        public ScannedItem(String item) {
            this.item = item;
        }
    }
}
