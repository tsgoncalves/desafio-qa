package kata09.supermarket;

public class BundlePrecoRegra implements PricingRule {
    private int unitPrice;
    private final int unitsPerBundle;
    private final int bundlePrice;

    public BundlePrecoRegra(int unitPrice, int unitsPerBundle, int bundlePrice) {
        this.unitPrice = unitPrice;
        this.unitsPerBundle = unitsPerBundle;
        this.bundlePrice = bundlePrice;
    }


    public int calcularpreco(int amount) {
        int bundles = amount / unitsPerBundle;
        int singleUnits = amount % unitsPerBundle;
        return bundles * bundlePrice + singleUnits * unitPrice;
    }
}
