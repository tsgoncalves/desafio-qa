package kata09.supermarket;


public class SinglePricingRule implements PricingRule {
    private int unitPrice;

    public SinglePricingRule(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int calcularpreco(int amount) {
        return unitPrice * amount;
    }
}
