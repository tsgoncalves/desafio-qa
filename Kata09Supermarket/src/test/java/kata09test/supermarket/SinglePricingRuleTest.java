package kata09test.supermarket;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import kata09.supermarket.PricingRule;
import kata09.supermarket.SinglePricingRule;

import static org.junit.Assert.assertThat;

public class SinglePricingRuleTest {

    @Test
    public void testCalculatePrice() throws Exception {
        PricingRule strategy = new SinglePricingRule(12);

        assertThat(strategy.calcularpreco(2), IsEqual.equalTo(24));
    }
}