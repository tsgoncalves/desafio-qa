package kata09test.supermarket;

import org.junit.Test;

import kata09.supermarket.BundlePrecoRegra;
import kata09.supermarket.Checkout;
import kata09.supermarket.RegraPreco;
import kata09.supermarket.SinglePricingRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

//http://codekata.com/kata/kata09-back-to-the-checkout/
public class CheckoutTest {
    @Test
    public void testCheckoutConsidersBundlesRegardlessOfOrder() throws Exception {
        Checkout checkout = new Checkout(createPricingRules());

        assertThat(checkout.getTotal(), equalTo(0));
        checkout.scan("A");
        assertThat(checkout.getTotal(), equalTo(50));
        checkout.scan("B");
        assertThat(checkout.getTotal(), equalTo(80));
        checkout.scan("A");
        assertThat(checkout.getTotal(), equalTo(130));
        checkout.scan("D");
        assertThat(checkout.getTotal(), equalTo(145));
        checkout.scan("A");
        assertThat("Checkout does not consider BundlePricingRule", checkout.getTotal(), equalTo(175));
    }

    private RegraPreco createPricingRules() {
        RegraPreco regraPreco = new RegraPreco();
        regraPreco.addRegra("A", new BundlePrecoRegra(50, 3, 130));
        regraPreco.addRegra("B", new BundlePrecoRegra(30, 2, 45));
        regraPreco.addRegra("C", new SinglePricingRule(20));
        regraPreco.addRegra("D", new SinglePricingRule(15));
        return regraPreco;
    }
}