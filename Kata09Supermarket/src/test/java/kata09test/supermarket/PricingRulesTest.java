package kata09test.supermarket;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import kata09.supermarket.BundlePrecoRegra;
import kata09.supermarket.RegraPreco;
import kata09.supermarket.SinglePricingRule;

import static org.junit.Assert.assertThat;

public class PricingRulesTest {
    @Test
    public void testPricingRules() throws Exception {
        RegraPreco regraPreco = new RegraPreco();
        regraPreco.addRegra("A", new BundlePrecoRegra(50, 3, 130));
        regraPreco.addRegra("B", new BundlePrecoRegra(30, 2, 45));
        regraPreco.addRegra("C", new SinglePricingRule(15));
        regraPreco.addRegra("D", new SinglePricingRule(15));

        assertThat(regraPreco.calcularPreco("A", 2), IsEqual.equalTo(100));
        assertThat(regraPreco.calcularPreco("A", 3), IsEqual.equalTo(130));
        assertThat(regraPreco.calcularPreco("B", 1), IsEqual.equalTo(30));
        assertThat(regraPreco.calcularPreco("B", 2), IsEqual.equalTo(45));
        assertThat(regraPreco.calcularPreco("B", 3), IsEqual.equalTo(75));
        assertThat(regraPreco.calcularPreco("D", 5), IsEqual.equalTo(75));
    }
}