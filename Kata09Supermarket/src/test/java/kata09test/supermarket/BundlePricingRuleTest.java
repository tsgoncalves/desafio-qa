package kata09test.supermarket;

import org.hamcrest.core.IsEqual;
import org.junit.Test;

import kata09.supermarket.BundlePrecoRegra;
import kata09.supermarket.PricingRule;

import static org.junit.Assert.assertThat;

public class BundlePricingRuleTest {

    @Test
    public void testCalculatePrice() throws Exception {
        PricingRule strategy = new BundlePrecoRegra(12, 3, 30);

        assertThat(strategy.calcularpreco(2), IsEqual.equalTo(24));
        assertThat(strategy.calcularpreco(3), IsEqual.equalTo(30));
        assertThat(strategy.calcularpreco(4), IsEqual.equalTo(42));
    }
}